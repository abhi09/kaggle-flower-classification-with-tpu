# FLOWER CLASSIFICATION WITH TPU
This project is an implementation of **TPU** with **tensorflow** for *flower classification*.

https://www.kaggle.com/c/flower-classification-with-tpus

It’s difficult to fathom just how vast and diverse our natural world is.

There are over 5,000 species of mammals, 10,000 species of birds, 30,000 species of fish – and astonishingly, over 400,000 different types of flowers.

In this competition, you’re challenged to build a machine learning model that identifies the type of flowers in a dataset of images (for simplicity, we’re sticking to just over 100 types).


# APPROACH
1. Used TPUs with Tensorflow
2. Densenet with Adam optimizer and 'sparse_categorical_crossentropy' loss
3. Used LearningRateScheduler to dicease the lr with epochs